//programa  que imprime un cuadrado cuyos marcos son 5*5
//me costo menos que el anterior XD
#include <stdio.h>
int main(int argc, char const *argv[]) {
  int A=5,B=3;
//******************************************, con esto separo los ciclos for :v
for (int i = 0; i < A; i++) {//inicia primer ciclo,donde imprime el primer marco horizontal
    printf("+" );
}
//******************************************
printf("\n");//salto de linea para los marcos verticales
//******************************************
for (int i =0; i < B; i++) {//inicia ciclo para los marcos verticales tipo +   +
    printf("+");
    printf(" ");
    printf(" ");
    printf(" ");
    printf("+");
    printf("\n");
  }
  //****************************************
for (int i = 0; i < A; i++) {//inicia tercer ciclo para el marco horizontal
    printf("+");
  }
  //****************************************
  printf("\n" );//salto de linea
  return 0;
}
